#!/bin/sh
# /sbin/setuser www-data - we initially run as root - nginx will switch to www-data

set -e
exec /var/lib/nginx/nginx -c /var/lib/nginx/conf/nginx.conf -p /var/lib/nginx/