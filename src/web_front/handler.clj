(ns web-front.handler
  (:require [compojure.core :refer [defroutes routes context POST GET]]
            [compojure.handler :as handler]
            [compojure.route :as route]
            [web-front.resources.jetstar-resource :refer [jetstar-flightbooking-event]]
            [web-front.resources.qantas-resource :refer [qantas-flightbooking-event]]
            [web-front.resources.heartbeat :refer [heartbeat]]
            [web-front.authr :refer [authenticated?]]
            [ring.middleware.basic-authentication :refer [wrap-basic-authentication]]
            [clojure.tools.logging :as log]))

(defn init []
  (log/info "web-front is starting with env: " (System/getenv "WEB_FRONT_CFG")))

(defn destroy []
  (log/info "web-front is shutting down"))

(defroutes public-routes
  (context "/heartbeat" [] heartbeat))

(defroutes protected-routes
  (context "/jetstar" [] jetstar-flightbooking-event)
  (context "/qantas" [] qantas-flightbooking-event)
  (route/not-found {:status 404}))

(defroutes app-routes
  public-routes
  (wrap-basic-authentication protected-routes authenticated?))

(def app
  (handler/api app-routes))
