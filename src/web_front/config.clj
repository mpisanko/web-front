(ns web-front.config
  (:require [clojure.edn :as edn]))

; config format (edn) - to be provided via WEB_FRONT_CFG env variable
; {
;   :credentials {:jetstar "ratstej" :qantas "satnaq"}
;   :redis {:host "localhost" :port 6379}
;   :queues {:qantas "queue:qantas" :jetstar "queue:jetstar"}
; }

(def cfg
  (edn/read-string (System/getenv "WEB_FRONT_CFG")))

(defn config [path]
  (get-in cfg path))

(defn redis-url []
  (let [host (get-in cfg [:redis :host] "localhost")
        port (get-in cfg [:redis :port] 6379)]
    {:url (str "redis://" host ":" port)}))