(ns web-front.queueing.jetstar
  (:require [web-front.config :refer [config]]
            [web-front.redis-client :refer [push-onto-queue]]))

(defn enqueue [event]
  (push-onto-queue (config [:queues :jetstar]) event))

