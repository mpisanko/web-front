(ns web-front.queueing.qantas
  (:require [web-front.config :refer [config]]
            [web-front.redis-client :refer [push-onto-queue]]))

(defn enqueue [event]
  (push-onto-queue (config [:queues :qantas]) event))

