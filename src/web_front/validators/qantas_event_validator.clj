(ns web-front.validators.qantas-event-validator
  (:require [cheshire.core :only [generate-string parse-string]]))

; FIXME - make sure message had been parsed from JSON
(defn event-valid? [context key-name]
  (let [message (get-in context [:request :body])]
    (if (= nil (get-in message [:request_id]))
      false
      true)))