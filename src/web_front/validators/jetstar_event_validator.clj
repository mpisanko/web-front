(ns web-front.validators.jetstar-event-validator)

(defn nil-or-empty? [object]
  (or (nil? object) (empty? object)))

(defn flight-valid? [flight]
  (let [departure [(get-in flight [:departure_airport_code]) (get-in flight [:departuring_at])]
        arrival [(get-in flight [:arrival_airport_code]) (get-in flight [:arriving_at])]
        passenger-types (map #(get-in % [:type]) (get-in flight [:passengers]))
        required-elements (concat departure arrival passenger-types)]
    (not-any? #(nil-or-empty? %) required-elements)))

(defn flights-present-and-valid? [flights]
  (and (< 0 (count flights)) (every? #(flight-valid? %) flights)))

(defn required-elements-valid? [required-elements]
  (not-any? #(nil-or-empty? %) required-elements))

(defn event-valid? [event]
  (let [flightbooking-event (get-in event [:flightbooking_event])
        flightbooking (first (get-in flightbooking-event [:flightbookings]))
        arranger (get-in flightbooking [:travel_arranger])
        flights (get-in flightbooking [:flights])
        required-elements [(get-in flightbooking-event [:request_id]) 
                           (get-in flightbooking-event [:type]) 
                           (get-in flightbooking-event [:created_at])
                           (get-in flightbooking [:created_at])
                           (get-in flightbooking [:pnr]) 
                           (get-in flightbooking [:payment_types])
                           (get-in arranger [:email])]]
      (and 
        (required-elements-valid? required-elements) 
        (flights-present-and-valid? flights))))
