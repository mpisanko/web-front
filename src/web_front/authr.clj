(ns web-front.authr
  (:require [web-front.config :refer [config]]
            [web-front.helpers.authentication-helper :refer [get-username-from-authorization-header]]))

(def credentials
  (config [:credentials]))

(defn authenticated? [user pass]
  (let [username (keyword user)]
    (and (contains? credentials username) (= pass (credentials username)))))

(defn executing-user [context]
  (get-username-from-authorization-header (get-in context [:request :headers "authorization"])))