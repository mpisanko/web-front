(ns web-front.resources.jetstar-resource
  (:require [web-front.validators.jetstar-event-validator :refer [event-valid?]]
            [web-front.helpers.body-helper :refer [body-as-string parse-json]]
            [web-front.queueing.jetstar :refer [enqueue]]
            [web-front.authr :refer [executing-user]]
            [liberator.core :refer [defresource]]
            [clojure.tools.logging :as log]))

(defn check-authorisation [context]
  (let [user (executing-user context)
        correct-user? (= "jetstar" user)]
    (do
      (log/info (format "request_authorised=%s" correct-user?))
      correct-user?)))

(defn malformed? [context]
  (try
    (let [message (body-as-string context)
          event (parse-json message)]
      [false {:event event :message message}])
    (catch Exception e
      (log/error (format "request_invalid_JSON=\"%s\"" (.getMessage e)))
      [true {:message (format "IOException: %s" (.getMessage e))}])))

(defn processable? [context]
  (let [event (:event context)
        valid? (event-valid? event)]
        (do
          (log/info (format "request_valid=%s" valid?))
          valid?)))

(defn created [context]
  (log/info (format "request_queued_guid='%s'" (get-in context [:event :flightbooking_event :request_id])))
  {:created "OK"})

(defresource jetstar-flightbooking-event
  :available-media-types ["application/json"]
  :allowed-methods [:post]
  :malformed? (fn [context] (malformed? context))
  :authorized? (fn [context] (check-authorisation context))
  :processable? (fn [context] (processable? context))
  :handle-created (fn [context] (created context))
  :post! (fn [context]
    (enqueue (:message context))))