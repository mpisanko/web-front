(ns web-front.resources.heartbeat
  (:require [web-front.redis-client :refer [ping]]
            [liberator.core :refer [defresource]]))

(defn status []
  (let [runtime-bean (java.lang.management.ManagementFactory/getRuntimeMXBean)
        memory-bean (java.lang.management.ManagementFactory/getMemoryMXBean)]
    (str "HEARTBEAT\nElasticache ping: " (ping)
          "\nHeap Memory: " (.getHeapMemoryUsage memory-bean) "\nNon Heap Memory: " (.getNonHeapMemoryUsage memory-bean)
          "\nUptime: " (/ (.getUptime runtime-bean) 60000) " minutes")))

(defresource heartbeat
  :available-media-types ["text/plain"]
  :allowed-methods [:get]
  :handle-ok (fn [context] (status)))