(ns web-front.resources.qantas-resource
  (:require [web-front.validators.qantas-event-validator :refer [event-valid?]]
            [web-front.helpers.body-helper :refer [body-as-string parse-json]]
            [web-front.queueing.qantas :refer [enqueue]]
            [liberator.core :refer [defresource]]))

(defresource qantas-flightbooking-event
  :available-media-types ["application/json"]
  :allowed-methods [:post] 
  :handle-created "created"
  :malformed? (complement (fn [context] (event-valid? (body-as-string context) :event)))
  ; FIXME - make sure get-event extracts the event from :request :body JSON
  :post! (fn [context] (enqueue (:event context))))