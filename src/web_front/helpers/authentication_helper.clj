(ns web-front.helpers.authentication-helper
  (:require [clojure.data.codec.base64 :as base64]))

(defn decode [encoded]
  (String. (base64/decode (.getBytes encoded))))

(defn get-username-from-authorization-header [header-value]
  (let [user-pass (second (clojure.string/split header-value #"\s+"))]
    (first (clojure.string/split (decode user-pass) #":"))))