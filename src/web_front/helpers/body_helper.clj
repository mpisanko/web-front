(ns web-front.helpers.body-helper
  (:require [cheshire.core :refer [parse-string]]))

(defn body-as-string [context]
  (if-let [body (get-in context [:request :body])]
    (condp instance? body
      java.lang.String body
      (slurp (clojure.java.io/reader body)))))

(defn parse-json [body]
  (parse-string body true))
