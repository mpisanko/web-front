(ns web-front.redis-client
  (:require [clj-redis.client :as redis]
            [web-front.config :refer [redis-url]]))

(def db
  (redis/init (redis-url)))

(defn push-onto-queue [qname value]
  (redis/lpush db qname value))

(defn pop-from-queue [qname]
  (redis/rpop db qname))

(defn ping []
  (redis/ping db))