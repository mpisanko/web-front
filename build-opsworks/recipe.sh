#!/bin/bash
wget --no-cookies --no-check-certificate --header "Cookie: gpw_e24=http%3A%2F%2Fwww.oracle.com%2F; oraclelicense=accept-securebackup-cookie" "http://download.oracle.com/otn-pub/java/jdk/8u5-b13/jdk-8u5-linux-x64.tar.gz"
tar xzf jdk-8u5-linux-x64.tar.gz && chown -R root:root jdk1.8.0_05 && mv jdk1.8.0_05 /usr/lib/jvm/java-8-oracle-amd64/ && rm -fR jdk-8u5-linux-x64.tar.gz && update-alternatives --install /usr/bin/java java /usr/lib/jvm/java-8-oracle-amd64/bin/java && update-alternatives --set java /usr/bin/java java /usr/lib/jvm/java-8-oracle-amd64/bin/java

wget http://apache.mirror.serversaustralia.com.au/tomcat/tomcat-7/v7.0.54/bin/apache-tomcat-7.0.54.tar.gz
tar xzf apache-tomcat-7.0.54.tar.gz
mv apache-tomcat-7.0.54/bin/* /usr/share/tomcat7/bin
mv apache-tomcat-7.0.54/lib/* /usr/share/tomcat7/lib

mkdir -p /var/log/web-front /var/log/tomcat7 /usr/share/tomcat7/temp /etc/nginx/main.d && chown -R tomcat7:www-data /var/log/web-front /var/log/tomcat7 /usr/share/tomcat7/temp && ln -s /etc/nginx/sites-available/web-front /etc/nginx/sites-enabled/web-front && ln -s /usr/share/tomcat7/bin /var/lib/tomcat7/bin && ln -s /usr/share/tomcat7/lib /var/lib/tomcat7/lib && rm -fR /var/lib/tomcat7/webapps/ROOT /etc/nginx/sites-available/default /etc/nginx/sites-enabled/default

