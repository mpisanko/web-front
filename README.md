web-front
=========

Running
=======
This application is configured using environment variable:

```WEB_FRONT_CFG='{:credentials {:jetstar "password" :qantas "password"} :redis {:host "localhost" :port 6379} :queues {:qantas "queue:qantas" :jetstar "queue:jetstar"}}'```

To run in embedded controller issue:

```lein ring server```

To run in a servlet container (eg Tomcat7) create an uberwar using

```lein ring uberwar```

and deploy into the container (drop resulting war into webapps directory)


In order to run in nginx-clojure use the following nginx.conf
```

user www-data;
daemon off;
worker_processes 1;
pid /run/nginx.pid;
env WEB_FRONT_CFG;

error_log  /var/log/nginx/error.log;

events {
    worker_connections  1024;
}


http {
    sendfile on;
    tcp_nopush on;
    tcp_nodelay on;
    keepalive_timeout 65;
    types_hash_max_size 2048;

    include       mime.types;
    default_type  application/octet-stream;

    log_format  main  '$remote_addr - $remote_user [$time_local] "$request" '
                      '$status $body_bytes_sent "$http_referer" '
                      '"$http_user_agent" "$http_x_forwarded_for"';

    access_log  /var/log/nginx/access.log main;

    gzip on;
    gzip_disable "msie6";

    gzip_vary on;
    gzip_proxied any;
    gzip_comp_level 6;
    gzip_buffers 16 8k;
    gzip_http_version 1.1;
    gzip_types text/plain text/css application/json application/x-javascript text/xml application/xml application/xml+rss text/javascript;

    jvm_path "/var/lib/jdk1.8.0_05/jre/lib/amd64/server/libjvm.so";
    jvm_options "-Djava.class.path=/var/lib/nginx/jars/nginx-clojure-0.2.3.jar:/var/lib/nginx/jars/web-front-0.1.0-SNAPSHOT-standalone.jar";
    jvm_options "-Xms1024m";
    jvm_options "-Xmx1024m";

    server {
        listen       80;
        server_name  localhost;


        access_log  /var/log/nginx/host.access.log;

        location /app {
          clojure;
          clojure_code '
          (do
              (use \'[web-front.handler])
              app))
          ';
       }

    }
}
```

Create a Docker image
=======
Build directory contains Dockerfile and files needed to be dropped into docker image. On top of that you'll need to copy web-front-0.1.0-SNAPSHOT-standalone.jar JAR file which you will find in target directory after issuing ```lein uberjar```

On a host which has docker installed issue the following command:
```
docker build -t <IMAGE_NAME> .
```