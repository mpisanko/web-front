#!/bin/sh
# /sbin/setuser www-data - we initially run as root - nginx will switch to www-data

set -e
exec /usr/sbin/nginx 