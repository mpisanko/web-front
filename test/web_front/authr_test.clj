(ns web-front.authr-test
  (:require [web-front.authr :refer :all]
            [clojure.test :refer :all]))

(deftest test-authr
  (testing "non existing user and empty password"
    (with-redefs [web-front.authr/credentials {:user1 "some password" :user2 "psswd"}]
      (is (= false (authenticated? "user123" nil))
        "Should not authenticate")))
    
  (testing "existing user with correct password"
    (with-redefs [web-front.authr/credentials {:user1 "some password" :user2 "psswd"}]
      (is (authenticated? "user2" "psswd")
        "Should authenticate"))))
