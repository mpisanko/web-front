(ns web-front.redis-client-test
  (:require [clojure.test :refer :all]
            [web-front.redis-client :refer :all]
            [web-front.config :refer [config]]))

(def q "some:queue")

(def messages [
  "abc"
  "abc:def , { asdh: 123 }"
  "{\"flightbooking_event\": {\"request_id\": \"abc123-456\", \"created_at\": \"2014-06-01T12:15:09\", \"type\": \"new\", 
    \"flightbookings\": [{\"created_at\": \"2014-06-01T12:15:09\", \"pnr\": \"XYZ666\", \"payment_types\": [\"credit card\"],
    \"travel_arranger\": {\"email\": \"me@example.org\"},\"flights\": []}]}}"
  "{
    \"flightbooking_event\": {\"request_id\": \"abc123-456\", \"created_at\": \"2014-06-01T12:15:09\", \"type\": \"new\", 
    \"flightbookings\": [{\"created_at\": \"2014-06-01T12:15:09\", \"pnr\": \"XYZ666\", \"payment_types\": [\"credit card\"],
    \"travel_arranger\": {\"email\": \"me@example.org\"},\"flights\": [{\"departure_airport_code\": \"MEL\",
    \"departuring_at\": \"2014-06-01T12:15:09\",\"arrival_airport_code\": \"SYD\",\"arriving_at\": \"2014-06-01T12:15:09\",
    \"passengers\": [{\"type\": \"adult\"},{\"type\": \"adult\"}]}]}]}}"
  ])

(defn push-some-messages []
  (push-onto-queue q (messages 0))
  (push-onto-queue q (messages 1))
  (push-onto-queue q (messages 2))
  (push-onto-queue q (messages 3))
  )

(deftest test-redis-client
  (testing "storing messages"
    (push-some-messages)
    (is (= (messages 0) (pop-from-queue q)))
    (is (= (messages 1) (pop-from-queue q)))
    (is (= (messages 2) (pop-from-queue q)))
    (is (= (messages 3) (pop-from-queue q)))
    (is (= 1 1))))
