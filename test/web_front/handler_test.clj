(ns web-front.handler-test
  (:use clojure.test
        ring.mock.request
        web-front.handler
        [web-front.redis-client :only [pop-from-queue]]
        [cheshire.core :only [generate-string parse-string]]
        [web-front.config :only [config]]))

(def some-date "2014-06-01T12:15:09")

(defn req [id] 
  {:flightbooking_event 
    {:type "new" :request_id (str "request-" id) :created_at some-date
      :flightbookings [
        {
          :created_at some-date
          :pnr (str "pnr-" id)
          :payment_types ["credit card"]
          :travel_arranger {:email "me@example.org"}
          :flights [
            {
              :departure_airport_code "MEL"
              :departuring_at some-date
              :arrival_airport_code "SYD"
              :arriving_at some-date
              :passengers [
                {
                  :type "adult"
                }
              ]
            }
          ]
        }
      ]}})

(defn invalid-request [id] 
  {:flightbooking_event 
    {:type "" :request_id (str "request-" id) :created_at some-date
      :flightbookings [
        {
          :created_at some-date
          :pnr ""
          :payment_types ["credit card"]
          :travel_arranger {:email "me@example.org"}
          :flights [
            {
              :departure_airport_code "MEL"
              :departuring_at some-date
              :arrival_airport_code "SYD"
              :arriving_at some-date
              :passengers [
                {
                  :type "adult"
                }
              ]
            }
          ]
        }
      ]}})

(defn to-json [request]
  (let [json (generate-string request)]
    json))

(defn from-json [structure]
  (parse-string structure true))

(defn authorisation-string [user]
  (let [auth (str user ":" (config [:credentials (keyword user)]))]
    (str "Basic " (String. (clojure.data.codec.base64/encode (.getBytes auth))))))

(deftest test-handler
  (testing "access requires authorisation"
    (let [response (app (request :post "/jetstar"))]
      (is (= (:status response) 401)
        "Should be 401")))

  (testing "not-found route with authorisation"
    (let [response (app (-> (request :post "/not-there") (header "authorization" (authorisation-string "jetstar"))))]
      (is (= (:status response) 404)
        "Should yield 404")))

  (testing "valid jetstar route with authorisation and body"
    (let [response1 (app (-> (request :post "/jetstar" (to-json (req 1))) 
                            (header "authorization" (authorisation-string "jetstar")) 
                            (content-type "application/json")))
          response2 (app (-> (request :post "/jetstar" (to-json (req 2))) 
                            (header "authorization" (authorisation-string "jetstar")) 
                            (content-type "application/json")))]
      (are [response] (= (:status response) 201)
        response1
        response2)

    (are [request] (= request (from-json (pop-from-queue "queue:jetstar")))
      ; the order is important - they should come out the same order they came in ;)
      (req 1)
      (req 2))))

  (testing "jetstar route with authorisation and malformed body"
    (let [response (app (-> (request :post "/jetstar" (to-json (invalid-request 3)))
                            (header "authorization" (authorisation-string "jetstar"))
                            (content-type "application/json")))]
      (is (= (:status response) 422))))

  ; (testing "valid qantas route with authorisation and body"
  ;   (let [response1 (app (-> (request :post "/qantas" (to-json {:request_id (str "request-" 9)})) 
  ;                           (header "authorization" (authorisation-string "user2")) 
  ;                           (content-type "application/json")))
  ;         response2 (app (-> (request :post "/qantas" (to-json {:request_id (str "request-" 8)})) 
  ;                           (header "authorization" (authorisation-string "user2")) 
  ;                           (content-type "application/json")))]
  ;     (is (= (:status response1) 201)
  ;       "Should be 201")
  ;     (is (= (:status response2) 201)
  ;       "Should be 201")
  ;     (is (= {:request_id (str "request-" 9)} (from-json (pop-from-queue "queue:qantas")))
  ;       "Should be the first request")
  ;     (is (= {:request_id (str "request-" 8)} (from-json (pop-from-queue "queue:qantas")))
  ;       "Should be the second request")))
  )
