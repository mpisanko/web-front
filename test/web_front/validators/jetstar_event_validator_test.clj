(ns web-front.validators.jetstar-event-validator-test
  (:require [web-front.validators.jetstar-event-validator :refer :all]
            [clojure.test :refer :all]
            [cheshire.core :refer [generate-string parse-string]]))

(def empty-request "{\"flightbooking_event\": {}}")

(def no-flights 
  "{
    \"flightbooking_event\": {
      \"request_id\": \"abc123-456\", 
      \"created_at\": \"2014-06-01T12:15:09\", 
      \"type\": \"new\", 
      \"flightbookings\": [
        {
          \"created_at\": \"2014-06-01T12:15:09\", 
          \"pnr\": \"XYZ666\", 
          \"payment_types\": [\"credit card\"],
          \"travel_arranger\": {\"email\": \"me@example.org\"},
          \"flights\": []
        }
      ]
    }
  }")


(def valid-event 
  "{
    \"flightbooking_event\": {
      \"request_id\": \"abc123-456\", 
      \"created_at\": \"2014-06-01T12:15:09\", 
      \"type\": \"new\", 
      \"flightbookings\": [
        {
          \"created_at\": \"2014-06-01T12:15:09\", 
          \"pnr\": \"XYZ666\", 
          \"payment_types\": [\"credit card\"],
          \"travel_arranger\": {\"email\": \"me@example.org\"},
          \"flights\": 
            [
              {
                \"departure_airport_code\": \"MEL\",
                \"departuring_at\": \"2014-06-01T12:15:09\",
                \"arrival_airport_code\": \"SYD\",
                \"arriving_at\": \"2014-06-01T12:15:09\",
                \"passengers\": 
                [
                  {
                    \"type\": \"adult\"
                  },
                  {
                    \"type\": \"adult\"
                  }
                ]
              }
            ]
        }
      ]
    }
  }")

(def no-pnr 
  "{
    \"flightbooking_event\": {
      \"request_id\": \"abc123-456\", 
      \"created_at\": \"2014-06-01T12:15:09\", 
      \"type\": \"new\", 
      \"flightbookings\": [
        {
          \"created_at\": \"2014-06-01T12:15:09\", 
          \"pnr\": \"\", 
          \"payment_types\": [\"credit card\"],
          \"travel_arranger\": {\"email\": \"me@example.org\"},
          \"flights\": 
            [
              {
                \"departure_airport_code\": \"MEL\",
                \"departuring_at\": \"2014-06-01T12:15:09\",
                \"arrival_airport_code\": \"SYD\",
                \"arriving_at\": \"2014-06-01T12:15:09\",
                \"passengers\": 
                [
                  {
                    \"type\": \"adult\"
                  },
                  {
                    \"type\": \"adult\"
                  }
                ]
              }
            ]
        }
      ]
    }
  }")

(def no-passenger-types 
  "{
    \"flightbooking_event\": {
      \"request_id\": \"abc123-456\", 
      \"created_at\": \"2014-06-01T12:15:09\", 
      \"type\": \"new\", 
      \"flightbookings\": [
        {
          \"created_at\": \"2014-06-01T12:15:09\", 
          \"pnr\": \"XYZ666\", 
          \"payment_types\": [\"credit card\"],
          \"travel_arranger\": {\"email\": \"me@example.org\"},
          \"flights\": 
            [
              {
                \"departure_airport_code\": \"MEL\",
                \"departuring_at\": \"2014-06-01T12:15:09\",
                \"arrival_airport_code\": \"SYD\",
                \"arriving_at\": \"2014-06-01T12:15:09\",
                \"passengers\": 
                [
                  {
                    \"title\": \"Mr\"
                  },
                  {
                    \"name\": \"Leo\"
                  }
                ]
              }
            ]
        }
      ]
    }
  }")

(defn parse [message]
  (parse-string message true))

(deftest test-invalid-inputs
  (testing "testing invalid events"
    (are [invalid-event] (= false (event-valid? (parse invalid-event)))
      empty-request
      no-flights
      no-pnr
      no-passenger-types)))

(deftest test-valid-inputs
  (testing "testing valid payload"
    (is (= true (event-valid? (parse valid-event))))))
