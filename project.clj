(defproject web-front "0.1.0-SNAPSHOT"
  :description "Simple API for receiving (JSON) requests and pushing them onto redis"
  :url "https://mpisanko@bitbucket.org/mpisanko/web-front.git"
  :dependencies [[org.clojure/clojure "1.6.0"]
                 [compojure "1.1.6"]
                 [hiccup "1.0.5"]
                 [ring-server "0.3.1"]
                 [ring/ring-json "0.3.1"]
                 [cheshire "5.3.1"]
                 [ring-basic-authentication "1.0.5"]
                 [org.clojure/data.codec "0.1.0"]
                 [clj-redis "0.0.12"]
                 [liberator "0.10.0"]
                 [org.clojure/tools.logging "0.3.0"]
                 [org.slf4j/slf4j-log4j12 "1.7.1"]
                 [log4j/log4j "1.2.17" :exclusions [javax.mail/mail
                                                    javax.jms/jms
                                                    com.sun.jmdk/jmxtools
                                                    com.sun.jmx/jmxri]]]
  :plugins [[lein-ring "0.8.10"]]
  :ring {:handler web-front.handler/app
         :init web-front.handler/init
         :destroy web-front.handler/destroy}
  :aot :all
  :profiles
  {:production
   {:ring
    {:open-browser? false, :stacktraces? false, :auto-reload? false}}
   :dev
   {:dependencies [[ring-mock "0.1.5"] [ring/ring-devel "1.2.1"]]}})
